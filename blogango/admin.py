from django.contrib import admin
from django import forms

from blogango.models import Blog, BlogEntry, Comment, BlogRoll, Reaction, BlogSocialLink

from sorl.thumbnail.admin import AdminImageMixin

from redactor.widgets import RedactorEditor

class BlogEntryAdminForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = BlogEntry
        widgets = {
            'text': RedactorEditor(),
        }
        
class BlogEntryAdmin(AdminImageMixin,admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    form = BlogEntryAdminForm
    
admin.site.register(Blog)
admin.site.register(BlogEntry, BlogEntryAdmin)
admin.site.register(Comment)
admin.site.register(BlogRoll)
admin.site.register(BlogSocialLink)
admin.site.register(Reaction)
