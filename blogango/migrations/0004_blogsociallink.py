# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import fontawesome.fields
import colorful.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blogango', '0003_blogentry_is_featured'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogSocialLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(unique=True)),
                ('text', models.CharField(max_length=100)),
                ('icon', fontawesome.fields.IconField(max_length=60, blank=True)),
                ('color', colorful.fields.RGBColorField(colors=[b'#005d9e', b'#6b2e1c', b'#44bbd0', b'#c3151a'])),
            ],
        ),
    ]
