# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blogango', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogentry',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'blog/images', blank=True),
        ),
    ]
