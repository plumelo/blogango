# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogango', '0002_blogentry_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogentry',
            name='is_featured',
            field=models.BooleanField(default=False),
        ),
    ]
